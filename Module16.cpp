#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <iomanip>
#include <time.h>


using namespace std;

void printArray(int array[5][5])
{
    for (int i = 0; i < 5; i++)
    {
        for (int j = 0; j < 5; j++)
        {
            cout << setw(3) << array[i][j];
        }
        cout << "\n";
    }
}


int main()
{
    int mainArray[5][5];

    for (int i = 0; i < 5; i++)
    {
        for (int j = 0; j < 5; j++)
        {
            mainArray[i][j] = i + j;
        }
    }

    printArray(mainArray);
    
    time_t t;
    tm* tk;
    time(&t);
    tk = localtime(&t);
    int day = tk->tm_mday;

    int ostDay = day % 5;
    int sum = 0;

    for (int i = 0; i < 5; i++)
    {
        sum += mainArray[ostDay - 1][i];
    }

    cout << "\n" << sum;
}
